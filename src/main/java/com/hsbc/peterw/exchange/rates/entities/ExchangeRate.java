package com.hsbc.peterw.exchange.rates.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDate;

@RequiredArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "exchange_rates", uniqueConstraints = @UniqueConstraint(columnNames={"baseCurrency", "currency", "exchangeRateDate"}))
public class ExchangeRate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String baseCurrency;

    private String currency;

    private LocalDate exchangeRateDate;

    private Double exchange;

    public ExchangeRate(final int id,
                        final String baseCurrency,
                        final String currency,
                        final LocalDate exchangeRateDate,
                        final Double exchange) {
        this.id = id;
        this.baseCurrency = baseCurrency;
        this.currency = currency;
        this.exchangeRateDate = exchangeRateDate;
        this.exchange = exchange;
    }

    @Override
    public boolean equals(Object object) {
        return EqualsBuilder.reflectionEquals(this, object);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
