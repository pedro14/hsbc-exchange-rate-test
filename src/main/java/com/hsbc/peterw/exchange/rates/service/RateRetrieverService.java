package com.hsbc.peterw.exchange.rates.service;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.HistoricalRates;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import com.hsbc.peterw.exchange.rates.entities.ExchangeRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.generated.restio.RatesApi;
import com.hsbc.peterw.exchange.rates.mapper.RatesMapper;
import com.hsbc.peterw.exchange.rates.repository.RateRepository;
import com.hsbc.peterw.exchange.rates.utils.DateUtils;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@Slf4j
public class RateRetrieverService {
    private final RateRepository rateRepository;
    private final RatesApi ratesApi;
    private final RatesMapper ratesMapper;
    private final DateUtils dateUtils;
    private final List<String> currency;
    private final String baseCurrency;
    private final List<Integer> range;
    private static final String DEFAULT_CURRENCY = "EUR";

    @Inject
    public RateRetrieverService(final RateRepository rateRepository,
                                final RatesApi ratesApi,
                                final RatesMapper ratesMapper,
                                final DateUtils dateUtils,
                                @Value("${application.currency:GBP,HKD,USD}") final List<String> currency,
                                @Value("${application.base.currency:EUR}") final String baseCurrency,
                                @Value("${application.historical.month.range:1,7}") final List<Integer> range) {
        this.rateRepository = rateRepository;
        this.ratesApi = ratesApi;
        this.ratesMapper = ratesMapper;
        this.dateUtils = dateUtils;
        this.currency = currency;
        this.baseCurrency = baseCurrency;
        this.range = range;
    }

    public LatestRates getLatestRates() {
        return ratesMapper.mapToRates(getRatesPerDay(dateUtils.getDateMinusWeekend(LocalDate.now())));
    }

    private List<ExchangeRate> getRatesPerDay(final LocalDate date) {
        final Optional<List<ExchangeRate>> exchangeRate =
                rateRepository.findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(date, baseCurrency, currency);
        if (exchangeRate.isPresent()) {
            return exchangeRate.get();
        }
        final List<ExchangeRate> exchangeRates = ratesMapper.mapToExchangeRateList(
                ratesApi.getByDate(date, baseCurrency.equalsIgnoreCase(DEFAULT_CURRENCY) ? null : baseCurrency, Collections.emptyList()));
        try {
            rateRepository.saveAll(exchangeRates);
        } catch (DataIntegrityViolationException e) {
            log.info("Failed to save exchange rates this can happen when data is weekend or slow to update on given date");
        }
        return filterRates(exchangeRates);
    }

    private List<ExchangeRate> filterRates(final List<ExchangeRate> exchangeRates) {
        return exchangeRates.stream()
                .filter(exchangeRate -> currency.contains(exchangeRate.getCurrency()))
                .collect(Collectors.toList());
    }

    public Historical getHistorical() {
        final List<LocalDate> dates = IntStream.range(range.get(0), range.get(1))
                .asLongStream()
                .mapToObj(month -> {
                    final LocalDate date = dateUtils.getDateMinusWeekend(LocalDate.now().minusMonths(month));
                    getRatesPerDay(date);
                    return date;
                })
                .collect(Collectors.toList());
        return new Historical().historicalRates(getExchangeRatesForDates(dates)).base(baseCurrency);
    }

    private List<HistoricalRates> getExchangeRatesForDates(final List<LocalDate> dates) {
        return currency.stream().map(currencyValue ->
                ratesMapper.mapToHistoricalRates(rateRepository.findByExchangeRateDateInAndCurrencyOrderByExchangeRateDateDesc(dates, currencyValue))
        ).filter(ObjectUtils::isNotEmpty).collect(Collectors.toList());
    }
}
