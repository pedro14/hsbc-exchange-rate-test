package com.hsbc.peterw.exchange.rates.ui.controller;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import com.hsbc.peterw.exchange.rates.ui.service.HistoricalRatesService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.inject.Inject;
import java.util.Optional;

@Controller
public class HistoricalController {
    private final HistoricalRatesService historicalRatesService;

    @Inject
    public HistoricalController(final HistoricalRatesService historicalRatesService) {
        this.historicalRatesService = historicalRatesService;
    }

    @ModelAttribute("historicalRates")
    public Historical historicalRates() {
        return new Historical();
    }

    @GetMapping(value = "/historical")
    @PreAuthorize("hasRole(USER)")
    public String getHistorical(final Model model) {
        final Optional<Historical> historicalOptional = historicalRatesService.getHistorical();
        if (historicalOptional.isPresent()) {
            model.addAttribute("historicalRates", historicalOptional.get());
        } else {
            model.addAttribute("error", "Unable to retrieve historical exchange rates. Please try again later.");
        }
        return "historical";
    }
}
