package com.hsbc.peterw.exchange.rates.ui.service;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import com.hsbc.peterw.exchange.rates.rest.RateV1Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Optional;

@Service
public class HistoricalRatesService {

    private final RateV1Api rateV1Api;

    @Inject
    public HistoricalRatesService(final RateV1Api rateV1Api) {
        this.rateV1Api = rateV1Api;
    }

    public Optional<Historical> getHistorical() {
        final ResponseEntity<Historical> historicalResponseEntity = rateV1Api.getHistorical();
        if (historicalResponseEntity.getStatusCode() == HttpStatus.OK) {
            return Optional.of(historicalResponseEntity.getBody());
        }
        return Optional.empty();
    }
}
