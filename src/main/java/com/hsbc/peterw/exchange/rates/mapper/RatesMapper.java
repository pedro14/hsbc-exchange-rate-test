package com.hsbc.peterw.exchange.rates.mapper;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.HistoricalRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.HistoricalRates;
import com.hsbc.peterw.exchange.rates.generated.restio.dto.Rates;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import com.hsbc.peterw.exchange.rates.entities.ExchangeRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class RatesMapper {
    public LatestRates mapRatesIO(final com.hsbc.peterw.exchange.rates.generated.restio.dto.Rates latest) {
        return new LatestRates().base(latest.getBase()).rates(mapRate(latest.getRates())).date(latest.getDate());
    }

    private List<LatestRate> mapRate(final Map<String, Double> rates) {
        return rates.entrySet().stream()
                .map(entry -> new LatestRate().rate(entry.getValue()).symbol(entry.getKey()))
                .collect(Collectors.toList());
    }

    public List<ExchangeRate> mapToExchangeRateList(final Rates rates) {
        return rates.getRates().entrySet().stream().map(rate ->
                ExchangeRate.builder()
                        .baseCurrency(rates.getBase())
                        .currency(rate.getKey())
                        .exchangeRateDate(LocalDate.parse(rates.getDate()))
                        .exchange(rate.getValue())
                        .build())
                .collect(Collectors.toList());
    }

    public LatestRates mapToRates(final List<ExchangeRate> exchangeRates) {
        final LatestRates rates = new LatestRates();
        exchangeRates.forEach(exchangeRate ->
                rates
                        .base(exchangeRate.getBaseCurrency())
                        .date(exchangeRate.getExchangeRateDate().format(DateTimeFormatter.ISO_LOCAL_DATE))
                        .addRatesItem(new LatestRate().rate(exchangeRate.getExchange()).symbol(exchangeRate.getCurrency())));
        return rates;
    }

    public HistoricalRates mapToHistoricalRates(final List<ExchangeRate> ratesPerDay) {
        if (ratesPerDay.isEmpty()) {
            return null;
        }
        return new HistoricalRates()
                .symbol(ratesPerDay.get(0).getCurrency())
                .rates(mapToHistoricalRate(ratesPerDay));
    }

    private List<HistoricalRate> mapToHistoricalRate(final List<ExchangeRate> rates) {
        return rates.stream()
                .map(rate -> new HistoricalRate()
                        .month(StringUtils.capitalize(rate.getExchangeRateDate().getMonth().name().toLowerCase()))
                        .day(rate.getExchangeRateDate().getDayOfMonth())
                        .rate(rate.getExchange()))
                .collect(Collectors.toList());
    }
}
