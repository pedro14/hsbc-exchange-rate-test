package com.hsbc.peterw.exchange.rates.repository;

import org.springframework.data.repository.CrudRepository;
import com.hsbc.peterw.exchange.rates.entities.ExchangeRate;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RateRepository extends CrudRepository<ExchangeRate, Long> {
    Optional<List<ExchangeRate>> findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(final LocalDate now, final String baseCurrency, final List<String> currency);

    int deleteByExchangeRateDateBefore(final LocalDate date);

    List<ExchangeRate> findByExchangeRateDateInAndCurrencyOrderByExchangeRateDateDesc(final List<LocalDate> dates, final String currency);
}
