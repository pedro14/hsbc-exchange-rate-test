package com.hsbc.peterw.exchange.rates.schedule;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.hsbc.peterw.exchange.rates.service.HistoricalRatesDeletionService;

import javax.inject.Inject;

@Service
@Slf4j
class HistoricalRatesTask {

    private final HistoricalRatesDeletionService historicalRatesDeletionService;

    @Inject
    public HistoricalRatesTask(final HistoricalRatesDeletionService historicalRatesDeletionService) {
        this.historicalRatesDeletionService = historicalRatesDeletionService;
    }

    @Scheduled(cron = "${application.historical.rates.cron}")
    void execute() {
        log.info("Running scheduled task to retrieve historical exchange rates");

        historicalRatesDeletionService.deleteHistoricalExchangeRates();
    }
}
