package com.hsbc.peterw.exchange.rates.ui.controller;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.ui.service.LatestRatesService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.inject.Inject;
import java.util.Optional;

@Controller
public class LatestController {
    private final LatestRatesService latestRatesService;

    @Inject
    public LatestController(final LatestRatesService latestRatesService) {
        this.latestRatesService = latestRatesService;
    }

    @ModelAttribute("latestRates")
    public LatestRates latestRates() {
        return new LatestRates();
    }

    @GetMapping({ "/", "/latest" })
    @PreAuthorize("hasRole(USER)")
    public String getLatest(final Model model)  {
        final Optional<LatestRates> latestRatesOptional = latestRatesService.getLatestRates();
        if (latestRatesOptional.isPresent()) {
            model.addAttribute("latestRates", latestRatesOptional.get());
        } else {
            model.addAttribute("error", "Unable to retrieve latest exchange rates. Please try again later.");
        }
        return "latest";
    }
}
