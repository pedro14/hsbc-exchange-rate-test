package com.hsbc.peterw.exchange.rates.rest;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.RatesApi;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.service.RateRetrieverService;

import javax.inject.Inject;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@Slf4j
public class RateV1Api implements RatesApi {

    private final RateRetrieverService rateRetrieverService;

    @Inject
    public RateV1Api(final RateRetrieverService rateRetrieverService) {
      this.rateRetrieverService = rateRetrieverService;
    }

    @Override
    @PreAuthorize("hasRole(USER)")
    public ResponseEntity<LatestRates> getLatest() {
        return ok().body(rateRetrieverService.getLatestRates());
    }

    @Override
    @PreAuthorize("hasRole(USER)")
    public ResponseEntity<Historical> getHistorical() {
        return ok().body(rateRetrieverService.getHistorical());
    }
}
