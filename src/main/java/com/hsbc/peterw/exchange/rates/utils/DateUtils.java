package com.hsbc.peterw.exchange.rates.utils;

import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;

@Component
public class DateUtils {
    public LocalDate getDateMinusWeekend(final LocalDate date) {
        switch (DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK))) {
            case SATURDAY:
                return date.minusDays(1);
            case SUNDAY:
                return date.minusDays(2);
            default:
                return date;
        }
    }
}
