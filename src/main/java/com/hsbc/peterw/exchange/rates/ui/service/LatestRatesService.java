package com.hsbc.peterw.exchange.rates.ui.service;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.rest.RateV1Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Optional;

@Service
public class LatestRatesService {

    private final RateV1Api rateV1Api;

    @Inject
    public LatestRatesService(final RateV1Api rateV1Api) {
        this.rateV1Api = rateV1Api;
    }

    public Optional<LatestRates> getLatestRates() {
        final ResponseEntity<LatestRates> latestRatesResponseEntity = rateV1Api.getLatest();
        if (latestRatesResponseEntity.getStatusCode() == HttpStatus.OK) {
            return Optional.of(latestRatesResponseEntity.getBody());
        }
        return Optional.empty();
    }
}
