package com.hsbc.peterw.exchange.rates.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hsbc.peterw.exchange.rates.repository.RateRepository;

import javax.inject.Inject;
import java.time.LocalDate;

@Service
public class HistoricalRatesDeletionService {

    private final RateRepository rateRepository;
    private final int monthsToKeep;

    @Inject
    public HistoricalRatesDeletionService(
            final RateRepository rateRepository,
            @Value("${application.historical.rates.months.to.keep:7}") final int monthsToKeep) {
        this.rateRepository = rateRepository;
        this.monthsToKeep = monthsToKeep;
    }

    @Transactional
    public void deleteHistoricalExchangeRates() {
        final LocalDate date = LocalDate.now().minusMonths(monthsToKeep);
        rateRepository.deleteByExchangeRateDateBefore(date);
    }
}
