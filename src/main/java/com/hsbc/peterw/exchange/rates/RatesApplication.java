package com.hsbc.peterw.exchange.rates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RatesApplication {

    public static void main(final String[] args) {
        SpringApplication.run(RatesApplication.class, args);
    }

}
