package com.hsbc.peterw.exchange.rates.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@Slf4j
public class RateControllerAdvice {

  private static final String TIME_KEY = "time";

  private static final String ERROR_KEY = "error";

  private static final String MESSAGE_KEY = "message";

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity handleStatusError(final MethodArgumentTypeMismatchException ex) {
    final String message = String.format("'%s' should be a valid '%s' and '%s' isn't",
            ex.getName(), ex.getRequiredType().getSimpleName(), ex.getValue());
    final  Map<String, String> error = new HashMap<>();
    error.put(TIME_KEY, Instant.now().toString());
    error.put(ERROR_KEY, "Argument mismatch");
    error.put(MESSAGE_KEY, message);
    log.warn(message, ex);
    return ResponseEntity.badRequest().body(error);
  }

  @ExceptionHandler(DateTimeParseException.class)
  public ResponseEntity handleDateError(final DateTimeParseException ex) {

    final Map<String, String> error = new HashMap<>();
    error.put(TIME_KEY, Instant.now().toString());
    error.put(ERROR_KEY, "Date Time parsing error");
    error.put(MESSAGE_KEY, ex.getMessage());
    log.warn(ex.getMessage(), ex);
    return ResponseEntity.badRequest().body(error);
  }

  @ExceptionHandler(RestClientException.class)
  public ResponseEntity handleRestClientException(final RestClientException ex) {
    log.warn("Handler for RestClientException triggered with message: {}", ex.getMessage(), ex);
    return ResponseEntity.status(HttpStatus.BAD_GATEWAY).build();
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity handleIllegalArgumentException(final IllegalArgumentException ex) {
    log.warn(
        "Handler for IllegalArgumentException triggered with message: {}", ex.getMessage(), ex);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
  }

  @ExceptionHandler(HttpMessageConversionException.class)
  public ResponseEntity handleHttpMessageConversionException(final HttpMessageConversionException ex) {
    log.warn(
        "Handler for HttpMessageConversionException triggered with message: {}",
        ex.getMessage(),
        ex);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity handleExceptionDefault(final Exception ex) {
    log.warn("Handler for Exception triggered with message: {}", ex.getMessage(), ex);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }
}
