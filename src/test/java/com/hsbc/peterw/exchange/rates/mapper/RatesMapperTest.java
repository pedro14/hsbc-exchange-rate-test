package com.hsbc.peterw.exchange.rates.mapper;

import com.hsbc.peterw.exchange.rates.entities.ExchangeRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.HistoricalRates;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.generated.restio.dto.Rates;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getExchangeRateListData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getHistoricalExchangeRateListData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getHistoricalRatesData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getLatestRatesForDateData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getRatesData;
import static org.assertj.core.api.Assertions.assertThat;

public class RatesMapperTest {
    private RatesMapper ratesMapper;
    private LatestRates testLatestRates;
    private List<ExchangeRate> testExchangeRateList;
    private List<ExchangeRate> testHistoricalExchangeRateList;
    private HistoricalRates testHistoricalRates;
    private Rates rates;

    @Before
    public void setUp() {
        ratesMapper = new RatesMapper();
        final LocalDate now = LocalDate.now();
        final String nowString = now.format(DateTimeFormatter.ISO_LOCAL_DATE);
        testLatestRates = getLatestRatesForDateData(nowString);
        testExchangeRateList = getExchangeRateListData();
        testHistoricalExchangeRateList = getHistoricalExchangeRateListData();
        testHistoricalRates = getHistoricalRatesData();
        rates = getRatesData();
    }

    @Test
    public void mapRatesIO() {
        final LatestRates latestRates = ratesMapper.mapRatesIO(rates);
        assertThat(latestRates).isEqualToComparingFieldByField(testLatestRates);
    }

    @Test
    public void mapToExchangeRateList() {
        final List<ExchangeRate> exchangeRateList = ratesMapper.mapToExchangeRateList(rates);
        assertThat(exchangeRateList).containsExactly(
                testExchangeRateList.get(0), testExchangeRateList.get(1), testExchangeRateList.get(2));
    }

    @Test
    public void mapToRates() {
        final LatestRates latestRates = ratesMapper.mapToRates(testExchangeRateList);
        assertThat(latestRates).isEqualTo(testLatestRates);
    }

    @Test
    public void mapToHistoricalRates() {
        final HistoricalRates historicalRates = ratesMapper.mapToHistoricalRates(testHistoricalExchangeRateList);
        assertThat(historicalRates).isEqualToComparingFieldByField(testHistoricalRates);
    }
}