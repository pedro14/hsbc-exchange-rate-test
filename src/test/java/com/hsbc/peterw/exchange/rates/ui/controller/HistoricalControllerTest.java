package com.hsbc.peterw.exchange.rates.ui.controller;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import com.hsbc.peterw.exchange.rates.ui.service.HistoricalRatesService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.Model;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HistoricalControllerTest {

    private HistoricalController historicalController;

    @Mock
    private HistoricalRatesService historicalRatesService;

    @Mock
    private Model model;

    @Before
    public void setUp() {
        historicalController = new HistoricalController(historicalRatesService);
    }

    @Test
    public void getHistorical() {
        when(historicalRatesService.getHistorical()).thenReturn(Optional.of(new Historical().base("EUR")));
        assertThat(historicalController.getHistorical(model), is("historical"));

        verify(model).addAttribute(eq("historicalRates"), any());
        verify(historicalRatesService, times(1)).getHistorical();
    }

    @Test
    public void getHistoricalFails() {
        when(historicalRatesService.getHistorical()).thenReturn(Optional.empty());
        assertThat(historicalController.getHistorical(model), is("historical"));

        verify(model).addAttribute("error", "Unable to retrieve historical exchange rates. Please try again later.");
        verify(historicalRatesService, times(1)).getHistorical();
    }
}