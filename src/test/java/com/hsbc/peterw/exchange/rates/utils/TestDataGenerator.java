package com.hsbc.peterw.exchange.rates.utils;

import com.hsbc.peterw.exchange.rates.entities.ExchangeRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.HistoricalRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.HistoricalRates;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.generated.restio.dto.Rates;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;

public class TestDataGenerator {
    public static final String EUR = "EUR";
    public static final String GBP = "GBP";
    public static final String USD = "USD";
    public static final String HKD = "HKD";
    public static final double VALUE_GBP = 1.03;
    public static final double VALUE_GBP_1 = 1.13;
    public static final double VALUE_GBP_2 = 1.23;
    public static final double VALUE_USD = 1.88;
    public static final double VALUE_HKD = 8.08;
    public static final String MARCH = "March";
    public static final String FEBRUARY = "February";
    public static final String JANUARY = "January";

    private TestDataGenerator() {
        //hide constructor
    }

    public static List<String> getCurrenciesData() {
        return Arrays.asList(GBP, HKD, USD);
    }

    public static LatestRates getLatestRatesData() {
        final LocalDate now = LocalDate.now();
        final String nowString = now.format(DateTimeFormatter.ISO_LOCAL_DATE);
        return getLatestRatesForDateData(nowString);
    }

    public static LatestRates getLatestRatesForDateData(final String date) {
        final LatestRate rateGBP = new LatestRate().rate(VALUE_GBP).symbol(GBP);
        final LatestRate rateUSD = new LatestRate().rate(VALUE_USD).symbol(USD);
        final LatestRate rateHKD = new LatestRate().rate(VALUE_HKD).symbol(HKD);
        return new LatestRates()
                .base(EUR)
                .date(date)
                .rates(Arrays.asList(rateGBP, rateUSD, rateHKD));
    }

    public static List<ExchangeRate> getExchangeRateListData() {
        final LocalDate now = LocalDate.now();
        return Arrays.asList(
                ExchangeRate.builder().baseCurrency(EUR).currency(GBP).exchange(VALUE_GBP).exchangeRateDate(now).build(),
                ExchangeRate.builder().baseCurrency(EUR).currency(USD).exchange(VALUE_USD).exchangeRateDate(now).build(),
                ExchangeRate.builder().baseCurrency(EUR).currency(HKD).exchange(VALUE_HKD).exchangeRateDate(now).build());
    }

    public static List<ExchangeRate> getHistoricalExchangeRateListData() {
        return Arrays.asList(
                ExchangeRate.builder().baseCurrency(EUR).currency(GBP).exchange(VALUE_GBP).exchangeRateDate(LocalDate.of(2020, 3, 22)).build(),
                ExchangeRate.builder().baseCurrency(EUR).currency(GBP).exchange(VALUE_GBP_1).exchangeRateDate(LocalDate.of(2020, 2, 22)).build(),
                ExchangeRate.builder().baseCurrency(EUR).currency(GBP).exchange(VALUE_GBP_2).exchangeRateDate(LocalDate.of(2020, 1, 22)).build());
    }

    public static Rates getRatesData() {
        final LocalDate now = LocalDate.now();
        final String nowString = now.format(DateTimeFormatter.ISO_LOCAL_DATE);
        final Map<String, Double> rate = new LinkedHashMap<>();
        rate.put(GBP, VALUE_GBP);
        rate.put(USD, VALUE_USD);
        rate.put(HKD, VALUE_HKD);
        return new Rates().base(EUR).date(nowString).rates(rate);
    }

    public static Historical getHistoricalData() {
        return new Historical().historicalRates(singletonList(getHistoricalRatesData())).base(EUR);
    }

    public static HistoricalRates getHistoricalRatesData() {
        final HistoricalRate historicalRateGBPMarch = new HistoricalRate().day(22).month(MARCH).rate(VALUE_GBP);
        final HistoricalRate historicalRateGBPFebruary = new HistoricalRate().day(22).month(FEBRUARY).rate(VALUE_GBP_1);
        final HistoricalRate historicalRateGBPJanuary = new HistoricalRate().day(22).month(JANUARY).rate(VALUE_GBP_2);
        return new HistoricalRates()
                .symbol(GBP)
                .rates(Arrays.asList(historicalRateGBPMarch, historicalRateGBPFebruary, historicalRateGBPJanuary));
    }
}
