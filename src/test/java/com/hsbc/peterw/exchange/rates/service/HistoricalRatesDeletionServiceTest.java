package com.hsbc.peterw.exchange.rates.service;

import com.hsbc.peterw.exchange.rates.repository.RateRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HistoricalRatesDeletionServiceTest {

    private HistoricalRatesDeletionService historicalRatesDeletionService;

    @Mock
    private RateRepository rateRepository;

    @Before
    public void setUp() {
        historicalRatesDeletionService = new HistoricalRatesDeletionService(rateRepository,7);
    }

    @Test
    public void deleteHistoricalExchangeRates() {
        historicalRatesDeletionService.deleteHistoricalExchangeRates();
        verify(rateRepository, times(1)).deleteByExchangeRateDateBefore(any());
    }
}