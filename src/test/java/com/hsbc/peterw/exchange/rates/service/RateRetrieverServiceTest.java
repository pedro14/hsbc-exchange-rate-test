package com.hsbc.peterw.exchange.rates.service;

import com.hsbc.peterw.exchange.rates.entities.ExchangeRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.HistoricalRate;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.generated.restio.RatesApi;
import com.hsbc.peterw.exchange.rates.generated.restio.dto.Rates;
import com.hsbc.peterw.exchange.rates.mapper.RatesMapper;
import com.hsbc.peterw.exchange.rates.repository.RateRepository;
import com.hsbc.peterw.exchange.rates.utils.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.EUR;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.GBP;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getCurrenciesData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getExchangeRateListData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getHistoricalExchangeRateListData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getLatestRatesData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getRatesData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RateRetrieverServiceTest {
    private RateRetrieverService rateRetrieverService;

    @Mock
    private RateRepository rateRepository;

    @Mock
    private RatesApi ratesApi;

    private LocalDate now;
    private List<String> currency;

    @Before
    public void setUp() {
        final RatesMapper ratesMapper = new RatesMapper();
        final DateUtils dateUtils = new DateUtils();
        currency = getCurrenciesData();
        now = dateUtils.getDateMinusWeekend(LocalDate.now());

        rateRetrieverService = new RateRetrieverService(
                rateRepository, ratesApi, ratesMapper, dateUtils,
                currency, EUR, Arrays.asList(1, 7));
    }

    @Test
    public void getLatestRatesTest() {
        final Rates rates = getRatesData();
        final LatestRates testLatestRates = getLatestRatesData();

        when(rateRepository.findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(now, EUR, currency)).thenReturn(Optional.empty());
        when(ratesApi.getByDate(now, null, Collections.emptyList())).thenReturn(rates);
        final LatestRates latestRates = rateRetrieverService.getLatestRates();
        assertThat(latestRates.getBase(), is(testLatestRates.getBase()));
        assertThat(latestRates.getDate(), is(testLatestRates.getDate()));
        assertThat(latestRates.getRates(), containsInAnyOrder(testLatestRates.getRates().get(0), testLatestRates.getRates().get(1), testLatestRates.getRates().get(2)));

        verify(ratesApi, times(1)).getByDate(any(), eq(null), anyList());
        verify(rateRepository, times(1)).findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(now, EUR, currency);
        verify(rateRepository, times(1)).saveAll(anyList());
    }

    @Test
    public void getLatestRatesInDatabase() {
        final LatestRates testLatestRates = getLatestRatesData();
        final List<ExchangeRate> testExchangeRateList = getExchangeRateListData();
        when(rateRepository.findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(now, EUR, currency)).thenReturn(Optional.of(testExchangeRateList));

        final LatestRates latestRates = rateRetrieverService.getLatestRates();
        assertThat(latestRates.getBase(), is(testLatestRates.getBase()));
        assertThat(latestRates.getDate(), is(testLatestRates.getDate()));
        assertThat(latestRates.getRates(), containsInAnyOrder(testLatestRates.getRates().get(0), testLatestRates.getRates().get(1), testLatestRates.getRates().get(2)));

        verify(ratesApi, times(0)).getByDate(any(), anyString(), anyList());
        verify(rateRepository, times(1)).findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(now, EUR, currency);
        verify(rateRepository, times(0)).saveAll(anyList());
    }

    @Test
    public void getHistoricalRates() {
        final List<ExchangeRate> testExchangeRateList = getExchangeRateListData();
        final List<ExchangeRate> testHistoricalExchangeRateList = getHistoricalExchangeRateListData();

        when(rateRepository.findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(any(), anyString(), anyList())).thenReturn(Optional.of(testExchangeRateList));
        when(rateRepository.findByExchangeRateDateInAndCurrencyOrderByExchangeRateDateDesc(anyList(), eq(GBP))).thenReturn(testHistoricalExchangeRateList);
        final Historical historical = rateRetrieverService.getHistorical();
        assertThat(historical.getHistoricalRates().size(), is(1));

        assertThat(historical.getBase(), is(testExchangeRateList.get(0).getBaseCurrency()));
        final List<HistoricalRate> historicalRateList = historical.getHistoricalRates().get(0).getRates();
        assertThat(historical.getHistoricalRates().get(0).getSymbol(), is(GBP));
        for (int i = 0; i < historicalRateList.size(); i++) {
            org.assertj.core.api.Assertions.assertThat(historicalRateList.get(i)).isEqualToComparingFieldByField(historicalRateList.get(i));
        }

        verify(ratesApi, times(0)).getByDate(any(), anyString(), anyList());
        verify(rateRepository, times(6)).findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(any(), anyString(), anyList());
        verify(rateRepository, times(0)).saveAll(anyList());
    }
}