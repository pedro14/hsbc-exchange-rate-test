package com.hsbc.peterw.exchange.rates.utils;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DateUtilsTest {

    private DateUtils dateUtils;

    @Before
    public void setUp() {
        dateUtils = new DateUtils();
    }

    @Test
    public void getDateMinusWeekendSunday() {
        final LocalDate date = LocalDate.of(2020, 4, 19);
        final LocalDate newDate = dateUtils.getDateMinusWeekend(date);
        assertThat(newDate.getYear(), is(2020));
        assertThat(newDate.getMonthValue(), is(4));
        assertThat(newDate.getDayOfMonth(), is(17));
    }

    @Test
    public void getDateMinusWeekendSaturday() {
        final LocalDate date = LocalDate.of(2020, 4, 18);
        final LocalDate newDate = dateUtils.getDateMinusWeekend(date);
        assertThat(newDate.getYear(), is(2020));
        assertThat(newDate.getMonthValue(), is(4));
        assertThat(newDate.getDayOfMonth(), is(17));
    }

    @Test
    public void getDateMinusWeekday() {
        final LocalDate date = LocalDate.of(2020, 4, 21);
        final LocalDate newDate = dateUtils.getDateMinusWeekend(date);
        assertThat(newDate.getYear(), is(2020));
        assertThat(newDate.getMonthValue(), is(4));
        assertThat(newDate.getDayOfMonth(), is(21));
    }
}