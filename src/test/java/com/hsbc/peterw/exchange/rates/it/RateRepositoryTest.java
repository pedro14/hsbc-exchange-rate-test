package com.hsbc.peterw.exchange.rates.it;

import com.hsbc.peterw.exchange.rates.entities.ExchangeRate;
import com.hsbc.peterw.exchange.rates.repository.RateRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.EUR;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.GBP;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.VALUE_GBP;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RateRepositoryTest {

    @Inject
    private RateRepository rateRepository;

    private ExchangeRate testExchangeRate;
    private LocalDate now;

    @Before
    public void setUp() {
        now = LocalDate.now();
        testExchangeRate = ExchangeRate.builder()
                .baseCurrency(EUR).currency(GBP).exchange(VALUE_GBP).exchangeRateDate(now).build();
        rateRepository.save(testExchangeRate);
    }

    @Test
    @Transactional
    @Rollback
    public void findByExchangeRateDateAndBaseCurrency() {
        final Optional<List<ExchangeRate>> exchangeRate =
                rateRepository.findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(now, EUR, singletonList(GBP));
        assertThat(exchangeRate.isPresent()).isEqualTo(true);
        assertThat(exchangeRate.get().get(0)).isEqualToComparingFieldByField(testExchangeRate);
    }

    @Test
    @Transactional
    @Rollback
    public void deleteByExchangeRateDateBefore() {
        rateRepository.deleteByExchangeRateDateBefore(now.plusDays(1));
        final Optional<List<ExchangeRate>> exchangeRate =
                rateRepository.findByExchangeRateDateAndBaseCurrencyAndCurrencyIn(now, EUR, singletonList(GBP));
        assertThat(exchangeRate.isPresent()).isEqualTo(false);
    }
}