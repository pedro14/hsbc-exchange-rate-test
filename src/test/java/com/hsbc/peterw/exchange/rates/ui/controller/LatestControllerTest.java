package com.hsbc.peterw.exchange.rates.ui.controller;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.ui.service.LatestRatesService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.Model;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LatestControllerTest {

    private LatestController latestController;

    @Mock
    private LatestRatesService latestRatesService;

    @Mock
    private Model model;

    @Before
    public void setUp() {
        latestController = new LatestController(latestRatesService);
    }
    @Test
    public void getLatest() {
        when(latestRatesService.getLatestRates()).thenReturn(Optional.of(new LatestRates().base("EUR")));
        assertThat(latestController.getLatest(model), is("latest"));

        verify(model).addAttribute(eq("latestRates"), any());
        verify(latestRatesService, times(1)).getLatestRates();
    }

    @Test
    public void getLatestFails() {
        when(latestRatesService.getLatestRates()).thenReturn(Optional.empty());
        assertThat(latestController.getLatest(model), is("latest"));

        verify(model).addAttribute("error", "Unable to retrieve latest exchange rates. Please try again later.");
        verify(latestRatesService, times(1)).getLatestRates();
    }
}