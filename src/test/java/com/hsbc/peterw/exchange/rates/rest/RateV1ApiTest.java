package com.hsbc.peterw.exchange.rates.rest;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.service.RateRetrieverService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getHistoricalData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getLatestRatesForDateData;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RateV1ApiTest {

    private RateV1Api rateV1Api;

    @Mock
    private RateRetrieverService rateRetrieverService;

    private LatestRates latestRates;

    private Historical historicalRates;

    @Before
    public void setUp() {
        rateV1Api = new RateV1Api(rateRetrieverService);
        latestRates = getLatestRatesForDateData("2020-04-20");
        historicalRates = getHistoricalData();
    }

    @Test
    public void getLatest() {
        when(rateRetrieverService.getLatestRates()).thenReturn(latestRates);
        assertThat(rateV1Api.getLatest()).isEqualToComparingFieldByField(ResponseEntity.ok().body(latestRates));
    }

    @Test
    public void getHistorical() {
        when(rateRetrieverService.getHistorical()).thenReturn(historicalRates);
        assertThat(rateV1Api.getHistorical()).isEqualToComparingFieldByField(ResponseEntity.ok().body(historicalRates));
    }
}