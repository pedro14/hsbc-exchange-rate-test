package com.hsbc.peterw.exchange.rates.ui.service;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import com.hsbc.peterw.exchange.rates.rest.RateV1Api;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HistoricalRatesServiceTest {

    private HistoricalRatesService historicalRatesService;

    @Mock
    private RateV1Api rateV1Api;

    @Before
    public void setUp() {
        historicalRatesService = new HistoricalRatesService(rateV1Api);
    }

    @Test
    public void getHistorical() {
        when(rateV1Api.getHistorical()).thenReturn(ResponseEntity.ok().body(new Historical().base("EUR")));
        final Optional<Historical> historicalOptional = historicalRatesService.getHistorical();
        assertThat(historicalOptional.isPresent(), is(true));
        assertThat(historicalOptional.get().getBase(), is("EUR"));

        verify(rateV1Api, times(1)).getHistorical();
    }

    @Test
    public void getHistoricalFails() {
        when(rateV1Api.getHistorical()).thenReturn(ResponseEntity.badRequest().body(null));
        assertThat(historicalRatesService.getHistorical().isPresent(), is(false));

        verify(rateV1Api, times(1)).getHistorical();
    }
}