package com.hsbc.peterw.exchange.rates.ui.service;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import com.hsbc.peterw.exchange.rates.rest.RateV1Api;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LatestRatesServiceTest {
    private LatestRatesService latestRatesService;

    @Mock
    private RateV1Api rateV1Api;

    @Before
    public void setUp() {
        latestRatesService = new LatestRatesService(rateV1Api);
    }

    @Test
    public void getLatestRates() {
        when(rateV1Api.getLatest()).thenReturn(ResponseEntity.ok().body(new LatestRates().base("EUR")));
        final Optional<LatestRates> latestRatesOptional = latestRatesService.getLatestRates();
        assertThat(latestRatesOptional.isPresent(), is(true));
        assertThat(latestRatesOptional.get().getBase(), is("EUR"));

        verify(rateV1Api, times(1)).getLatest();
    }

    @Test
    public void getLatestRatesFails() {
        when(rateV1Api.getLatest()).thenReturn(ResponseEntity.badRequest().body(null));
        assertThat(latestRatesService.getLatestRates().isPresent(), is(false));

        verify(rateV1Api, times(1)).getLatest();
    }
}