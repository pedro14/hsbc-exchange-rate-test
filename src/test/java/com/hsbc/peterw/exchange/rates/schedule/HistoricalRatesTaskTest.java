package com.hsbc.peterw.exchange.rates.schedule;

import com.hsbc.peterw.exchange.rates.service.HistoricalRatesDeletionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HistoricalRatesTaskTest {
    
    private HistoricalRatesTask historicalRatesTask;

    @Mock
    private HistoricalRatesDeletionService historicalRatesDeletionService;

    @Before
    public void setUp() {
        historicalRatesTask = new HistoricalRatesTask(historicalRatesDeletionService);
    }

    @Test
    public void execute() {
        historicalRatesTask.execute();
        verify(historicalRatesDeletionService, times(1)).deleteHistoricalExchangeRates();
    }
}