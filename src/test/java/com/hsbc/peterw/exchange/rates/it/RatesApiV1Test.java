package com.hsbc.peterw.exchange.rates.it;

import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.Historical;
import com.hsbc.peterw.exchange.rates.generated.rest.v1.dto.LatestRates;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.hsbc.peterw.exchange.rates.rest.RateV1Api;
import com.hsbc.peterw.exchange.rates.service.RateRetrieverService;

import javax.inject.Inject;

import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getHistoricalData;
import static com.hsbc.peterw.exchange.rates.utils.TestDataGenerator.getLatestRatesForDateData;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RatesApiV1Test {
    private static final String V_1_LATEST = "/v1/latest";
    private static final String V_1_HISTORICAL = "/v1/historical";

    @Inject
    protected RateV1Api rateV1Api;

    @MockBean
    protected RateRetrieverService rateRetrieverService;

    @Inject
    private MockMvc mockMvc;

    private LatestRates latestRates;

    private Historical historicalRates;

    @Before
    public void setUp() {
        latestRates = getLatestRatesForDateData("2020-04-20");
        historicalRates = getHistoricalData();
    }

    @Test
    @WithMockUser(username = "admin")
    public void getLatestReturnsNoRates() throws Exception {
        when(rateRetrieverService.getLatestRates()).thenReturn(null);
        mockMvc
                .perform(get(V_1_LATEST))
                .andExpect(status().isOk())
                .andExpect(content().string(""));

        verify(rateRetrieverService, times(1)).getLatestRates();
    }

    @Test
    @WithMockUser(username = "admin")
    public void getLatestReturnsRates() throws Exception {
        when(rateRetrieverService.getLatestRates()).thenReturn(latestRates);
        mockMvc
                .perform(get(V_1_LATEST))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"base\":\"EUR\",\"date\":\"2020-04-20\",\"rates\":[{\"symbol\":\"GBP\",\"rate\":1.03},{\"symbol\":\"USD\",\"rate\":1.88},{\"symbol\":\"HKD\",\"rate\":8.08}]}"));

        verify(rateRetrieverService, times(1)).getLatestRates();
    }

    @Test
    public void getLatestNotLoggedIn() throws Exception {
        when(rateRetrieverService.getLatestRates()).thenReturn(latestRates);
        mockMvc
                .perform(get(V_1_LATEST))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));

        verify(rateRetrieverService, times(0)).getLatestRates();
    }

    @Test
    @WithMockUser(username = "admin")
    public void getHistoricalReturnsNoHistoricalRates() throws Exception {
        when(rateRetrieverService.getHistorical()).thenReturn(null);
        mockMvc
                .perform(get(V_1_HISTORICAL))
                .andExpect(status().isOk())
                .andExpect(content().string(""));

        verify(rateRetrieverService, times(1)).getHistorical();
    }

    @Test
    @WithMockUser(username = "admin")
    public void getHistoricalReturnsHistoricalRates() throws Exception {
        when(rateRetrieverService.getHistorical()).thenReturn(historicalRates);
        mockMvc
                .perform(get(V_1_HISTORICAL))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"base\":\"EUR\",\"historicalRates\":[{\"symbol\":\"GBP\",\"rates\":[{\"month\":\"March\",\"day\":22,\"rate\":1.03},{\"month\":\"February\",\"day\":22,\"rate\":1.13},{\"month\":\"January\",\"day\":22,\"rate\":1.23}]}]}"));

        verify(rateRetrieverService, times(1)).getHistorical();
    }

    @Test
    public void getHistoricalNotLoggedIn() throws Exception {
        when(rateRetrieverService.getHistorical()).thenReturn(historicalRates);
        mockMvc
                .perform(get(V_1_HISTORICAL))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));

        verify(rateRetrieverService, times(0)).getHistorical();
    }
}
