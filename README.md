# Exchange Rates service

## Assesment for role with HSBC

### Build/Starting the Service

Start on port 8080

```mvn clean test spring-boot:run```

### Access the latest exchange rates user/password admin/admin
http://localhost:8080/latest

### Access the historical exchange rates for previous six months user/password admin/admin
http://localhost:8080/historical

