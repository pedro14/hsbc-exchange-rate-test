FROM gcr.io/distroless/java:11
ADD target/rates*.jar /rates.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/rates.jar"]

